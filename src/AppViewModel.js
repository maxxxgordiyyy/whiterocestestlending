import { stringsRU } from "@/assets/strings";
import {AppEvent, passAppEvent} from "@/components/events/Event";

const defaultNavItems = [
    stringsRU.navMenuAboutComplex,
    stringsRU.navMenuInfrastructure,
    stringsRU.navMenuPrepositions,
    stringsRU.navMenuApartmentSelection,
    stringsRU.navMenuGuarantees,
    stringsRU.navMenuEnvironment,
    stringsRU.navMenuGallery,
    stringsRU.navMenuPaymentMethods,
    stringsRU.navMenuDocuments,
    stringsRU.navMenuAboutCompany
]

export class AppViewModel {

    constructor() {
        this.navigationMenuItems = defaultNavItems
        this.isNavigationMenuOpened = false
    }

    navigationClick(text) {
        passAppEvent(AppEvent.SCROLL_TO, text)
    }
}