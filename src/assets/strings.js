const stringsUntranslatable = {
    phoneNumber: "8 (861) 204-39-82"
}

const stringsRU = {
    mainWhiteRoces: 'ЖК "Белые росы"',
    mainResidentalQuarter: 'Жилой квартал',
    mainOrderCall: 'Заказать звонок',
    navMenuAboutComplex: 'О комплексе',
    navMenuInfrastructure: 'Инфраструктура',
    navMenuPrepositions: 'Преимущества',
    navMenuApartmentSelection : 'Подбор квартиры',
    navMenuGuarantees: 'Гарантии',
    navMenuEnvironment: 'Окружение',
    navMenuGallery: 'Галерея',
    navMenuPaymentMethods: 'Способы оплаты',
    navMenuDocuments: 'Документы',
    navMenuAboutCompany: 'О компании'
};

const stringsEN = {
    mainResidentalQuarter: 'Living quarter',
    mainOrderCall: 'Order a call'
};

//TODO: Fonts: Forum, OpenSans, Circe

export { stringsRU, stringsEN, stringsUntranslatable };