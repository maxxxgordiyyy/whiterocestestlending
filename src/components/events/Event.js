import EventBus from "@/components/events/EventBus";

export const AppEvent = {
    NAVIGATION_CLICK: "navigation",
    SCROLL_TO: "scrollTo"
}

export function passAppEvent(event, associatedValue = null) {
    EventBus.$emit(event, associatedValue)
}